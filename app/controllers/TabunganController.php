<?php

class TabunganController extends BaseController {

	public function tabunganApi($idNasabah = 1){
		$data = DB::table('tabungan')
		->join('nasabah', 'nasabah.id', '=', 'tabungan.nasabah_id')
		->select('nasabah.id','tabungan.created_at as tanggal', 'tabungan.tranksaksi','tabungan.debit', 'tabungan.kredit', 'tabungan.saldo_sementara as saldo')
		->where('nasabah.id', '=', $idNasabah)
		->orderBy('tabungan.created_at', 'ASC');
		$hasil = $data->get();
		$jumlah = $data->count();
		$tabungan = $data->paginate(100);
		$no = 1;

		$data = new Illuminate\Database\Eloquent\Collection($hasil);

		return Datatable::collection($data)
		->addColumn('no', function($model){
			$no = 1;
			return $no++;
		})
		->addColumn('tanggal', function($model){
			$unix = strtotime($model->tanggal);
			$bulan = date("m", $unix);
			$bulan = str_replace('01', 'Januari', $bulan);
			$bulan = str_replace('02', 'Februari', $bulan);
			$bulan = str_replace('03', 'Maret', $bulan);
			$bulan = str_replace('04', 'April', $bulan);
			$bulan = str_replace('05', 'Mai', $bulan);
			$bulan = str_replace('06', 'Juni', $bulan);
			$bulan = str_replace('07', 'Juli', $bulan);
			$bulan = str_replace('08', 'Agustus', $bulan);
			$bulan = str_replace('09', 'September', $bulan);
			$bulan = str_replace('10', 'Oktober', $bulan);
			$bulan = str_replace('11', 'November', $bulan);
			$bulan = str_replace('12', 'Desember', $bulan);
			return $date_timestamps = date('d', $unix) . ' ' .$bulan . ' ' . date('Y', $unix);
		})
		->addColumn('tranksaksi', function($model){
			return $model->tranksaksi;
		})
		->addColumn('debit', function($model){
			if ($model->debit != NULL) {
				return "Rp ". number_format($model->debit,0,',','.');
			} else {
				return '-';
			}
		})
		->addColumn('kredit', function($model){
			if ($model->kredit != NULL) {
				return "Rp ". number_format($model->kredit,0,',','.');
			} else {
				return '-';
			}
		})
		->addColumn('saldo', function($model){
			return "Rp ". number_format($model->saldo,0,',','.');
		})
		->make();
	}

	public function getAddTabungan() {

	}

	public function postAddTabungan() {

		$users = DB::table('nasabah_sampahah')
		->select(DB::raw('count(*) as user_count, status'))
		->where('status', '<>', 1)
		->groupBy('status')
		->get();

		$tabungan = new Tabungan;
		$tabungan->nasabah_id = Input::get('nasabah_id');
		$tabungan->debit = Input::get('debit');
		$tabungan->kredit = Input::get('kredit');
		$tabungan->save();

		return Redirect::back()->with('successMessage', 'Tabungan baru telah berhasi dibuat');
	}

}

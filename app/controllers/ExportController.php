<?php 

/**
* 
*/
class ExportController extends BaseController
{
	public function tabunganToPdf($id){
		$nasabah = Nasabah::find($id);

		$data = DB::table('tabungan')
		->join('nasabah', 'nasabah.id', '=', 'tabungan.nasabah_id')
		->select('nasabah.id','tabungan.created_at', 'tabungan.tranksaksi','tabungan.debit', 'tabungan.kredit', 'tabungan.saldo_sementara as saldo')
		->where('nasabah.id', '=', $id )
		->orderBy('tabungan.created_at', 'ASC')->get();

		$view = View::make('export.pdf.tabungan')->with('data', $data)->with('nasabah', $nasabah)->render();
		$pdf = PDF::loadHTML($view); 

		$nama = 'rch-tabungan-'.$nasabah->no_rek.'-'.date('dmy').'-'.rand(00,99);

		// return $pdf->download($nama.'.pdf');
		return $pdf->stream();
	}

	public function nasabahToPdf(){
		$data = Nasabah::all();
		$tanggalCetak = date('d m Y H:i:s');
		$no = 1;

		$view = View::make('export.pdf.nasabah')->with('data', $data)
												->with('tanggalCetak', $tanggalCetak)
												->with('no', $no)
												->render();
		$pdf = PDF::loadHTML($view); 

		$nama = 'rch-nasabah-'.date('dmy').'-'.rand(00,99);

		// return $pdf->download($nama.'.pdf');
		return $pdf->stream();
	}
}
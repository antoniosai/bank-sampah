<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tabungan extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tabungan', function($table){
			$table->increments('id');
			$table->integer('nasabah_id');
			$table->string('debit');
			$table->string('kredit');
			$table->string('tranksaksi');
			$table->integer('saldo_sementara');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tabungan');
	}

}

@extends('backend.templates.teller')

@section('style_head')
	@include('partials.datatable')
@stop

@section('title')
	Edit Sampah
@stop

@section('nav2')
active
@stop

@section('header')
Edit Sampah {{ $sampah->nama }}
@stop

@section('content')
<form action="{{ action('SampahController@postEditSampah') }}" method="post">
	<input type="hidden" name="id" value="{{ $sampah->id}}">
	<div class="form-group">
		<label for="nama">Nama Sampah</label>
		<input type="name" name="nama" value="{{ $sampah->nama}}" class="form-control">
	</div>
	<div class="form-group">
		<label for="harga">Harga Sampah</label>
		<div class="input-group">
			<div class="input-group-addon">Rp. </div><input type="number" name="harga" value="{{ $sampah->harga }}" class="form-control">
		</div>
	</div>
	<input type="submit" name="submit" value="Simpan" class="btn btn-success">
</form>
@stop

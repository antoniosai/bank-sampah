
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Dinamically Row With JQuery</title>
    <link rel="stylesheet" href="style.css">
    <script type="text/javascript" src="{{ asset('js/jquery-1.8.3.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.append.js') }}"></script>
  </head>
  <body>
    <table>
      <thead>
        <tr>
          <th>No</th>
          <th>Tranksaksi</th>
          <th>Debit</th>
          <th>Kredit</th>
          <th>Saldo Sementara</th>
        </tr>
      </thead>
      <tbody>
        <?php $no = 1; ?>
        @foreach($data as $nasabah)
        <tr>
          <td>{{ $no++ }}</td>
          <td>{{ $nasabah->tranksaksi }}</td>
          <td>{{ $nasabah->debit }}</td>
          <td>{{ $nasabah->kredit }}</td>
          <td>{{ $nasabah->saldo_sementara }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </body>
</html>

<!DOCTYPE html>
<html>
<head>
	<title>Selamat Datang di Bank Sampah</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('styles/css/bootstrap.min.css') }}">
	<script type="text/javascript" src="{{ asset('styles/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('styles/js/jquery-1.8.3.min.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			var count = 1;

			$("#add_btn").click(function(){
				count += 1;
				$('#isi').append(
					'<tr class="records">'
					+ '<td ><div id="'+count+'">' + count + '</div></td>'
					+ '<td><div class="form-group"><select name="sampah_'+count+'" class="form-control" required><option value="all">-- Semua Status --</option>@foreach ($sampah as $data)<option value="{{ $data->nama }}">{{ $data->nama }}</option>@endforeach</select></div></td>'
					+ '<td style="width: 30%"><div class="form-group"><input class="form-control" type="number" name="qty_'+count+'" value="" placeholder="Ex. 0.4, 0.5, 1.5" step="any" required></div></td>'
					+ '<td><div class="form-group"><select name="satuan_'+count+'" class="form-control" required><option value="kg">kg</option><option value="g">g</option></select></div></td>'
					+ '<td><a class="remove_item btn btn-danger btn-md" href="#" >Delete</a>'
					+ '<input id="rows_' + count + '" name="rows[]" value="'+ count +'" type="hidden"></td></tr>'
					);
			});

			$(".remove_item").live('click', function (ev) {
				if (ev.type == 'click') {
					$(this).parents(".records").fadeOut();
					$(this).parents(".records").remove();
				}
			});
		});
	</script>
</head>
<?php 
	
	function format_rupiah($val){
		echo "Rp ". $rupiah=number_format($val,0,',','.');
	}

?>
<body class="container">
	<h1>Selamat Datang di RCH Bank Sampah</h1>
	@include('partials.alert')
	<a href="#" class="btn btn-default">Beranda</a>
	<a href="#" class="btn btn-success">Simulasi Harga Sampah</a>
	<a href="#" class="btn btn-warning">Tentang</a>

	<div style="margin: auto; float: right">
		<a href="{{ action('UserController@getLogin') }}" class="btn">Login</a>
	</div>
	<hr>
	<div class="col-md-8">
	<br/>
		<form role="form" method="get" action="">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>No</th>
						<th>Sampah</th>
						<th style="width: 120px">Berat</th>
						<th>Satuan</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody id="isi">
					<tr>
						<td>1</td>
						<td>
							<div class="form-group">
								<select name="sampah_1" class="form-control" required>
									<option name="sampah_1"  value="all">-- Pilih Sampah --</option>
									@foreach ($sampah as $data)
									<option value="{{ $data->nama }}">{{ $data->nama }}</option>
									<!-- <input type="hidden" name="harga_1" value="{{ $data->harga }}"></input> -->
									@endforeach
								</select>
							</div>
						</td>
						<td>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<input class="form-control" type="number" name="qty_1" value="" placeholder="Ex. 0.4, 0.5, 1.5" step="any" required>
									</div>
								</div>
							</div>
						</td>
						<td>
							<div class="form-group">
								<select name="satuan_1" class="form-control" required>
									<option value="kg">kg</option>
									<option value="g">g</option>
								</select>
							</div>
						</td>
						<td><a class="remove_item btn btn-danger btn-md disabled" href="#" >Delete</a></td>
					</tr>
				</tbody>
			</table>
			<hr>
			<input id="rows_1" name="rows[]" value="1" type="hidden"></td></tr>
			<input type="button" class="btn btn-info" name="add_btn" value="Tambah Baris" id="add_btn">
			<div style="margin: auto; float: right">
				<input type="submit" name="submit" class="btn btn-success" value="Hitung">
			</div>
		</form>
	</div>
	<div class="col-md-4">
		<h3>Daftar Harga Sampah</h3>
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>Harga/kg</th>
				</tr>
			</thead>
			<tbody>
				@foreach($sampah as $data)
				<tr>
					<td>{{ $no++ }}</td>
					<td>{{ $data->nama }}</td>
					<td>{{ format_rupiah($data->harga) }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	@if(Input::get('submit'))
	<h3>Hasil Simulasi Perhitungan</h3>
	<hr>
	<table class="table">
		<thead>
			<tr>
				<td>Sampah</td>
				<td>Berat</td>
				<td>Harga/kg</td>
				<td>Sub Total</td>
			</tr>
		</thead>
		<tbody>
			@foreach (Input::get('rows') as $key => $count )
			<?php 
			$sampah = Sampah::find(Input::get('sammpah_'.$count));
			?>
			<tr>
				<td>{{ Input::get('sampah_'.$count) }}</td>
				<td>{{ Input::get('qty_'.$count) }} kg</td>
				<td></td>
				<td></td>
			</tr>
			@endforeach
		</tbody>
	</table>
	@endif
</body>
</html>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<!-- <link rel="stylesheet" type="text/css" href="{{ asset('styles/css/bootstrap.min.css') }}"> -->
</head>
<body>
	<table>
		<thead>
			<tr>
				<th style="width: 120px">No</th>
				<th>No Rek</th>
				<th>Nama</th>
				<th>Telp</th>
				<th>Alamat</th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $nasabah)
			<tr>
				<td>{{ $no++ }}</td>
				<td>{{ $nasabah->no_rek }}</td>
				<td>{{ $nasabah->nama }}</td>
				<td>{{ $nasabah->no_telp }}</td>
				<td>{{ $nasabah->alamat }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>
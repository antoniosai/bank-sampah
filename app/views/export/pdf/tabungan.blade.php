<!DOCTYPE html>
<html>
<head>
	<title></title>
	<!-- <link rel="stylesheet" type="text/css" href="http://localhost/bank-sampah-styles/css/bootstrap.min.css"> -->
	<style type="text/css">
		/* Table */ .table { margin: 1px; text-align:left; } th { border-bottom: 1px solid #333; font-weight: bold; } .td { border-bottom: 1px solid #333; } th,td { padding: 4px 10px 4px 0; } tfoot { font-style: italic; } caption { background: #fff; margin-bottom:2em; text-align:left; } thead {display: table-header-group;} img,tr {page-break-inside: avoid;}

		body {
			font-family: Arial;
			font-size: 15px;
		}
	</style>
</head>
<body>
	<?php

	function format_rupiah($val){
		echo "Rp ". number_format($val,0,',','.');
	}
	
	function format_bulan($val){
		$unix = strtotime($val);
		$bulan = date("m", $unix);
		$bulan = str_replace('01', 'Januari', $bulan);
		$bulan = str_replace('02', 'Februari', $bulan);
		$bulan = str_replace('03', 'Maret', $bulan);
		$bulan = str_replace('04', 'April', $bulan);
		$bulan = str_replace('05', 'Mei', $bulan);
		$bulan = str_replace('06', 'Juni', $bulan);
		$bulan = str_replace('07', 'Juli', $bulan);
		$bulan = str_replace('08', 'Agustus', $bulan);
		$bulan = str_replace('09', 'September', $bulan);
		$bulan = str_replace('10', 'Oktober', $bulan);
		$bulan = str_replace('11', 'November', $bulan);
		$bulan = str_replace('12', 'Desember', $bulan);

		echo $date_timestamps =  date('d', $unix) . ' ' .$bulan . ' ' . date('Y', $unix);
	}

	function format_hari($val){
		$unix = strtotime($val);
		$hari = date("D", $unix);
		$hari = str_replace('Sun', 'Minggu', $hari);
		$hari = str_replace('Mon', 'Senin', $hari);
		$hari = str_replace('Tue', 'Selasa', $hari);
		$hari = str_replace('Wed', 'Rabu', $hari);
		$hari = str_replace('Thu', 'Kamis', $hari);
		$hari = str_replace('Fri', 'Jum\'at', $hari);
		$hari = str_replace('Sat', 'Sabtu', $hari);
	}
	?>
	<table>
		<tr>
			<td>No. Rek</td>
			<td>:</td>
			<td>{{ $nasabah->no_rek }}</td>

			<td style="padding-left: 40px">TTL</td>
			<td>:</td>
			<td>{{$nasabah->tempat_lahir}}, {{ format_bulan($nasabah->tanggal_lahir) }}</td>
		</tr>
		<tr>
			<td>Nama</td>
			<td>:</td>
			<td>{{ $nasabah->nama }}</td>

			<td style="padding-left: 40px">Telp.</td>
			<td>:</td>
			<td>{{ $nasabah->no_telp }}</td>
		</tr>
	</table>
	<br>
	<table class="table">
		<thead>
			<tr>
				<th style="width: 120px">Tanggal</th>
				<th>Tranksaksi</th>
				<th>Debit</th>
				<th>Kredit</th>
				<th>Saldo</th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $tabungan)
			<tr>
				<td align="center" class="td">{{ format_bulan($nasabah->created_at) }}</td>
				<td class="td">{{ $tabungan->tranksaksi }}</td>
				<td align="center" class="td">
					<?php if ($tabungan->debit == NULL || $tabungan->debit == 0): ?>
						{{ '-'; }}
					<?php else: ?>
						{{ format_rupiah($tabungan->debit) }}
					<?php endif; ?>
				</td>
				<td align="center" class="td">
					<?php if ($tabungan->kredit == NULL || $tabungan->kredit == 0): ?>
						{{ '-'; }}
					<?php else: ?>
						{{ format_rupiah($tabungan->kredit) }}
					<?php endif; ?>
				</td>
				<td align="center" class="td">
					{{ format_rupiah($tabungan->saldo) }}
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	<br/>	
	<p style="margin: auto; float: right">Tanggal cetak : <?php echo format_bulan(date('d-m-Y')) ?></p>
</body>
</html>
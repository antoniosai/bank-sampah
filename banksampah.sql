-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 12, 2016 at 05:45 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

CREATE DATABASE banksampah;
USE banksampah;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bank-sampah`
--

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'administrator', 'Administrator', '{"admin":1}', '2016-05-08 18:44:28', '2016-05-08 18:44:28'),
(2, 'operator', 'Teller', '{"operator":1}', '2016-05-08 18:44:28', '2016-05-08 18:44:28');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_sampah`
--

CREATE TABLE `jenis_sampah` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_04_07_050553_migration_cartalyst_sentry_install_users', 1),
('2016_04_07_050554_migration_cartalyst_sentry_install_groups', 1),
('2016_04_07_050555_migration_cartalyst_sentry_install_users_groups_pivot', 1),
('2016_04_07_050556_migration_cartalyst_sentry_install_throttle', 1),
('2016_04_08_161631_nasabah', 1),
('2016_04_08_161700_sampah', 1),
('2016_04_08_161908_jenis_sampah', 1),
('2016_04_08_164236_nasabah_sampah', 1),
('2016_04_12_104527_tabungan', 1),
('2016_04_15_132409_Saldo', 1);

-- --------------------------------------------------------

--
-- Table structure for table `nasabah`
--

CREATE TABLE `nasabah` (
  `id` int(10) UNSIGNED NOT NULL,
  `no_rek` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tempat_lahir` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal_lahir` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `saldo` int(11) NOT NULL,
  `alamat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `no_telp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nasabah`
--

INSERT INTO `nasabah` (`id`, `no_rek`, `nama`, `tempat_lahir`, `tanggal_lahir`, `saldo`, `alamat`, `no_telp`, `created_at`, `updated_at`) VALUES
(1, '169431', 'Antonio Saiful Islam', 'Garut', '1996-09-09', 40737, 'Enhaka Residence Blok C7', '08121494007', '2016-05-08 18:44:56', '2016-05-09 09:19:04'),
(2, '169498', 'Diky Anggara', 'Bandung', '1991-11-12', 7875, 'Garut', '085723456052', '2016-05-08 20:48:21', '2016-05-10 07:36:39'),
(3, '168842', 'Anton', 'Garut', '1996-09-09', 979660, 'Suci Permai', '087878268445', '2016-05-09 09:01:12', '2016-05-09 09:13:28');

-- --------------------------------------------------------

--
-- Table structure for table `nasabah_sampah`
--

CREATE TABLE `nasabah_sampah` (
  `id` int(10) UNSIGNED NOT NULL,
  `nasabah_id` int(10) UNSIGNED NOT NULL,
  `sampah_id` int(10) UNSIGNED NOT NULL,
  `qty` float(8,2) NOT NULL,
  `price` float(8,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nasabah_sampah`
--

INSERT INTO `nasabah_sampah` (`id`, `nasabah_id`, `sampah_id`, `qty`, `price`, `created_at`, `updated_at`) VALUES
(26, 1, 1, 0.96, 1152.00, '2016-05-09 08:37:22', '2016-05-09 08:37:22'),
(27, 1, 2, 0.15, 225.00, '2016-05-09 08:38:05', '2016-05-09 08:38:05'),
(28, 1, 1, 1.50, 1800.00, '2016-05-09 08:38:05', '2016-05-09 08:38:05'),
(29, 1, 1, 0.80, 960.00, '2016-05-09 08:46:09', '2016-05-09 08:46:09'),
(30, 3, 1, 2.00, 2400.00, '2016-05-09 09:08:09', '2016-05-09 09:08:09'),
(31, 3, 2, 0.80, 1200.00, '2016-05-09 09:08:09', '2016-05-09 09:08:09'),
(32, 3, 3, 1.00, 1900.00, '2016-05-09 09:08:09', '2016-05-09 09:08:09'),
(33, 3, 2, 0.80, 1200.00, '2016-05-09 09:10:21', '2016-05-09 09:10:21'),
(34, 3, 1, 0.80, 960.00, '2016-05-09 09:10:57', '2016-05-09 09:10:57'),
(35, 3, 1, 800.00, 960000.00, '2016-05-09 09:10:57', '2016-05-09 09:10:57'),
(36, 3, 1, 0.80, 960.00, '2016-05-09 09:10:58', '2016-05-09 09:10:58'),
(37, 3, 1, 1.00, 1200.00, '2016-05-09 09:12:45', '2016-05-09 09:12:45'),
(38, 3, 1, 1.00, 1200.00, '2016-05-09 09:12:46', '2016-05-09 09:12:46'),
(39, 3, 1, 1.00, 1200.00, '2016-05-09 09:12:46', '2016-05-09 09:12:46'),
(40, 3, 2, 1.20, 1800.00, '2016-05-09 09:13:27', '2016-05-09 09:13:27'),
(41, 3, 1, 0.20, 240.00, '2016-05-09 09:13:27', '2016-05-09 09:13:27'),
(42, 1, 2, 2.00, 3000.00, '2016-05-09 09:14:58', '2016-05-09 09:14:58'),
(43, 1, 1, 3.00, 3600.00, '2016-05-09 09:14:59', '2016-05-09 09:14:59'),
(44, 1, 2, 1.00, 1500.00, '2016-05-09 09:14:59', '2016-05-09 09:14:59'),
(45, 1, 3, 4.00, 7600.00, '2016-05-09 09:15:47', '2016-05-09 09:15:47'),
(46, 1, 1, 2.00, 2400.00, '2016-05-09 09:15:47', '2016-05-09 09:15:47'),
(47, 1, 2, 1.00, 1500.00, '2016-05-09 09:15:47', '2016-05-09 09:15:47'),
(48, 1, 2, 2.00, 3000.00, '2016-05-09 09:19:03', '2016-05-09 09:19:03'),
(49, 1, 2, 4.00, 6000.00, '2016-05-09 09:19:03', '2016-05-09 09:19:03'),
(50, 1, 2, 1.00, 1500.00, '2016-05-09 09:19:03', '2016-05-09 09:19:03'),
(51, 2, 2, 3.00, 4500.00, '2016-05-09 09:31:42', '2016-05-09 09:31:42'),
(52, 2, 2, 2.00, 3000.00, '2016-05-09 09:31:43', '2016-05-09 09:31:43'),
(53, 2, 2, 0.25, 375.00, '2016-05-10 07:36:38', '2016-05-10 07:36:38');

-- --------------------------------------------------------

--
-- Table structure for table `sampah`
--

CREATE TABLE `sampah` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `harga` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sampah`
--

INSERT INTO `sampah` (`id`, `nama`, `harga`, `created_at`, `updated_at`) VALUES
(1, 'Pelastik', 1200, '2016-05-08 18:45:16', '2016-05-09 07:26:37'),
(2, 'Kardus', 1500, '2016-05-08 20:55:07', '2016-05-08 20:55:07'),
(3, 'Kaca', 1900, '2016-05-09 09:04:46', '2016-05-09 09:05:16');

-- --------------------------------------------------------

--
-- Table structure for table `tabungan`
--

CREATE TABLE `tabungan` (
  `id` int(10) UNSIGNED NOT NULL,
  `sampah_id` int(10) UNSIGNED DEFAULT NULL,
  `nasabah_id` int(10) UNSIGNED NOT NULL,
  `debit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kredit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tranksaksi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `saldo_sementara` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tabungan`
--

INSERT INTO `tabungan` (`id`, `sampah_id`, `nasabah_id`, `debit`, `kredit`, `tranksaksi`, `saldo_sementara`, `created_at`, `updated_at`) VALUES
(27, 1, 1, '1152', '', 'Deposit sampah Pelastik sebanyak 0.96 kg @Rp1.200', 1152, '2016-05-09 08:37:22', '2016-05-09 08:37:22'),
(28, 2, 1, '225', '', 'Deposit sampah Kardus sebanyak 0.15 kg @Rp1.500', 1377, '2016-05-09 08:38:05', '2016-05-09 08:38:05'),
(29, 1, 1, '2025', '', 'Deposit sampah Pelastik sebanyak 1.5 kg @Rp1.200', 3177, '2016-05-09 08:38:05', '2016-05-09 08:38:05'),
(30, NULL, 1, '', '3000', 'Kredit', 177, '2016-05-09 08:44:50', '2016-05-09 08:44:50'),
(31, NULL, 1, '', '100', 'Kredit', 77, '2016-05-09 08:45:57', '2016-05-09 08:45:57'),
(32, 1, 1, '960', '', 'Deposit sampah Pelastik sebanyak 0.8 kg @Rp1.200', 1037, '2016-05-09 08:46:09', '2016-05-09 08:46:09'),
(33, 1, 3, '2400', '', 'Deposit sampah Pelastik sebanyak 2 kg @Rp1.200', 2400, '2016-05-09 09:08:09', '2016-05-09 09:08:09'),
(34, 2, 3, '3600', '', 'Deposit sampah Kardus sebanyak 0.8 kg @Rp1.500', 3600, '2016-05-09 09:08:09', '2016-05-09 09:08:09'),
(35, 3, 3, '5500', '', 'Deposit sampah Kaca sebanyak 1 kg @Rp1.900', 5500, '2016-05-09 09:08:09', '2016-05-09 09:08:09'),
(36, 2, 3, '1200', '', 'Deposit sampah Kardus sebanyak 0.8 kg @Rp1.500', 6700, '2016-05-09 09:10:21', '2016-05-09 09:10:21'),
(37, 1, 3, '960', '', 'Deposit sampah Pelastik sebanyak 0.8 kg @Rp1.200', 7660, '2016-05-09 09:10:57', '2016-05-09 09:10:57'),
(38, 1, 3, '960960', '', 'Deposit sampah Pelastik sebanyak 800 kg @Rp1.200', 967660, '2016-05-09 09:10:58', '2016-05-09 09:10:58'),
(39, 1, 3, '961920', '', 'Deposit sampah Pelastik sebanyak 0.8 kg @Rp1.200', 968620, '2016-05-09 09:10:58', '2016-05-09 09:10:58'),
(40, 1, 3, '1200', '', 'Deposit sampah Pelastik sebanyak 1 kg @Rp1.200', 969820, '2016-05-09 09:12:46', '2016-05-09 09:12:46'),
(41, 1, 3, '2400', '', 'Deposit sampah Pelastik sebanyak 1 kg @Rp1.200', 972220, '2016-05-09 09:12:46', '2016-05-09 09:12:46'),
(42, 1, 3, '3600', '', 'Deposit sampah Pelastik sebanyak 1 kg @Rp1.200', 975820, '2016-05-09 09:12:46', '2016-05-09 09:12:46'),
(43, 2, 3, '1800', '', 'Deposit sampah Kardus sebanyak 1.2 kg @Rp1.500', 977620, '2016-05-09 09:13:27', '2016-05-09 09:13:27'),
(44, 1, 3, '2040', '', 'Deposit sampah Pelastik sebanyak 0.2 kg @Rp1.200', 979660, '2016-05-09 09:13:27', '2016-05-09 09:13:27'),
(45, 2, 1, '3000', '', 'Deposit sampah Kardus sebanyak 2 kg @Rp1.500', 4037, '2016-05-09 09:14:59', '2016-05-09 09:14:59'),
(46, 1, 1, '6600', '', 'Deposit sampah Pelastik sebanyak 3 kg @Rp1.200', 10637, '2016-05-09 09:14:59', '2016-05-09 09:14:59'),
(47, 2, 1, '8100', '', 'Deposit sampah Kardus sebanyak 1 kg @Rp1.500', 18737, '2016-05-09 09:14:59', '2016-05-09 09:14:59'),
(48, 3, 1, '7600', '', 'Deposit sampah Kaca sebanyak 4 kg @Rp1.900', 26337, '2016-05-09 09:15:47', '2016-05-09 09:15:47'),
(49, 1, 1, '10000', '', 'Deposit sampah Pelastik sebanyak 2 kg @Rp1.200', 28737, '2016-05-09 09:15:47', '2016-05-09 09:15:47'),
(50, 2, 1, '11500', '', 'Deposit sampah Kardus sebanyak 1 kg @Rp1.500', 30237, '2016-05-09 09:15:47', '2016-05-09 09:15:47'),
(51, 2, 1, '3000', '', 'Deposit sampah Kardus sebanyak 2 kg @Rp1.500', 33237, '2016-05-09 09:19:03', '2016-05-09 09:19:03'),
(52, 2, 1, '6000', '', 'Deposit sampah Kardus sebanyak 4 kg @Rp1.500', 39237, '2016-05-09 09:19:03', '2016-05-09 09:19:03'),
(53, 2, 1, '1500', '', 'Deposit sampah Kardus sebanyak 1 kg @Rp1.500', 40737, '2016-05-09 09:19:03', '2016-05-09 09:19:03'),
(54, 2, 2, '4500', '', 'Deposit sampah Kardus sebanyak 3 kg @Rp1.500', 4500, '2016-05-09 09:31:43', '2016-05-09 09:31:43'),
(55, 2, 2, '3000', '', 'Deposit sampah Kardus sebanyak 2 kg @Rp1.500', 7500, '2016-05-09 09:31:43', '2016-05-09 09:31:43'),
(56, 2, 2, '375', '', 'Deposit sampah Kardus sebanyak 0.25 kg @Rp1.500', 7875, '2016-05-10 07:36:39', '2016-05-10 07:36:39');

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE `throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `throttle`
--

INSERT INTO `throttle` (`id`, `user_id`, `ip_address`, `attempts`, `suspended`, `banned`, `last_attempt_at`, `suspended_at`, `banned_at`) VALUES
(1, 1, '::1', 0, 0, 0, NULL, NULL, NULL),
(2, 2, '::1', 0, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `permissions`, `activated`, `activation_code`, `activated_at`, `last_login`, `persist_code`, `reset_password_code`, `first_name`, `last_name`, `created_at`, `updated_at`) VALUES
(1, 'admin@mail.com', '$2y$10$zhPllYDJvAs/FS7SvYBFyOTDoUwo7jd5IMJoizZie7CLa61TMX.2a', NULL, 1, NULL, '2016-05-08 18:44:28', '2016-05-11 12:20:17', '$2y$10$mAe9Q9xtHaBugRjFzpRzrevxLSwy09C1HLvoZXL.KuZPpUqxzVbju', NULL, 'Administrator', 'Bank Sampah', '2016-05-08 18:44:28', '2016-05-11 12:20:17'),
(2, 'antoniosaiful10@gmail.com', '$2y$10$v3d75b983HvEd14M1Aw0bOcJcPxXimAd2ajUNG1eED7M8ALJpJqqW', NULL, 1, NULL, '2016-05-08 18:44:28', '2016-05-11 12:23:25', '$2y$10$s.8YQiRkclRz13Yt6s/X4OkMlR07N3LwnPI5GkTxCxp3DmxhWYIU.', NULL, 'Operator', 'Bank Sampah', '2016-05-08 18:44:28', '2016-05-11 12:23:25');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`user_id`, `group_id`) VALUES
(1, 1),
(2, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `groups_name_unique` (`name`);

--
-- Indexes for table `jenis_sampah`
--
ALTER TABLE `jenis_sampah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nasabah`
--
ALTER TABLE `nasabah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nasabah_sampah`
--
ALTER TABLE `nasabah_sampah`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nasabah_sampah_nasabah_id_index` (`nasabah_id`),
  ADD KEY `nasabah_sampah_sampah_id_index` (`sampah_id`);

--
-- Indexes for table `sampah`
--
ALTER TABLE `sampah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabungan`
--
ALTER TABLE `tabungan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_tabungan_sampah` (`sampah_id`),
  ADD KEY `FK_tabungan_nasabah` (`nasabah_id`);

--
-- Indexes for table `throttle`
--
ALTER TABLE `throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `throttle_user_id_index` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_activation_code_index` (`activation_code`),
  ADD KEY `users_reset_password_code_index` (`reset_password_code`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`user_id`,`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `jenis_sampah`
--
ALTER TABLE `jenis_sampah`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nasabah`
--
ALTER TABLE `nasabah`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `nasabah_sampah`
--
ALTER TABLE `nasabah_sampah`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `sampah`
--
ALTER TABLE `sampah`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tabungan`
--
ALTER TABLE `tabungan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `throttle`
--
ALTER TABLE `throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `nasabah_sampah`
--
ALTER TABLE `nasabah_sampah`
  ADD CONSTRAINT `nasabah_sampah_nasabah_id_foreign` FOREIGN KEY (`nasabah_id`) REFERENCES `nasabah` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `nasabah_sampah_sampah_id_foreign` FOREIGN KEY (`sampah_id`) REFERENCES `sampah` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tabungan`
--
ALTER TABLE `tabungan`
  ADD CONSTRAINT `FK_tabungan_nasabah` FOREIGN KEY (`nasabah_id`) REFERENCES `nasabah` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_tabungan_sampah` FOREIGN KEY (`sampah_id`) REFERENCES `sampah` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
